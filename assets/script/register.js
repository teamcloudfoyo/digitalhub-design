$('#submit').click(function(){
	if(!validateEmail($('#email').val())) {
		alert('Please enter a valid email');
		return;
	}

	if($('#name').val() === '') {
		alert('Please enter a name');
		return;
	}

	if($('#company').val() === '') {
		alert('Please enter a company name');
		return;
	}

	if($('#branch').val() === '') {
		alert('Please enter a branch name');
		return;
	}

	if($('#department').val() === '') {
		alert('Please enter a department name');
		return;
	}

	if($('#password').val() === '') {
		alert('Please enter a valid password');
		return;
	}

	if($('#password').val().length < 6) {
		alert('Password must be atleast 6 characters');
		return;
	}

	if($('#password').val() !== $('#password-confirm').val()) {
		alert('Passwords do not match');
		return;
	}

	$.ajax({
		url : baseUrl + '/user',
		type : 'POST',
		data : {
			first_name : $('#name').val(),
			email : $('#email').val(),
			password : $('#password').val(),
			company : $('#company').val(),
			branch : $('#branch').val(),
			department : $('#department').val()
		},
		success : function(result){
			alert('Regstration successful. Please login to your account');
			window.location = "login.html";
		},
		error: function (error){
    		console.log(error);
			var keysbyindex = Object.keys(error.responseJSON.msg);
			if(keysbyindex.length > 0)
				alert(error.msg[keysbyindex[0]][0])
			else
				alert('An error occured please try again.');
    	}
	});
});


var validateEmail = function(email) {
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
        return false;
    }
    return true;
}
